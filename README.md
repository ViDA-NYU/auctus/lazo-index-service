# Lazo Index Service

The Lazo Index Service is a search server, which allow you to search high-dimensional data for intersection and containment. For example, it can quickly find datasets with categorical features that intersect with some input data.

It wraps the [Lazo](https://github.com/mitdbg/lazo) library as a gRPC server, allowing you to index data and then run queries against the same in-memory index from multiple clients.

## Server

### Building from Source

First, install [Apache Maven](https://maven.apache.org/). Then, run the following:

    $ ./install-repositories
    $ mvn package

You can then run the server like so:

    $ java -jar target/lazo_index-0.7.2-jar-with-dependencies.jar -p <port> -db <database> ...

where `<database>` is the database used by the Lazo Index Server (see [below](#supported-databases)), and `<port>` is the port where the Lazo Index Server will be running. You might need more arguments depending on the database being used (see [below](#supported-databases)).

### Using Docker

We publish pre-built docker images on [GitLab](https://gitlab.com/ViDA-NYU/auctus/lazo-index-service/container_registry). You can run the latest image using:

    $ docker run -e PORT=<port> -e DATABASE=<database> ... registry.gitlab.com/vida-nyu/auctus/lazo-index-service:0.7.2

The image can be built from the included Dockerfile:

    $ docker build -t IMAGE .

### Supported Databases

#### Elasticsearch

We provide support for [Elasticsearch 7.10.2](https://www.elastic.co/downloads/past-releases/elasticsearch-7-10-2). To use Elasticsearch as the database, the arguments are the following:

    $ java -jar target/lazo_index-0.7.2-jar-with-dependencies.jar -p <port> -db elasticsearch -eh <elasticsearch_host> -ep <elasticsearch_port>

where `<elasticsearch_host>` and `<elasticsearch_port>` are the hostname and the port where the Elasticsearch server is running, respectively.

If you are running on a Docker image:

    $ docker run -e PORT=<port> -e DATABASE=elasticsearch -e ELASTICSEARCH_HOST=<elasticsearch_host> -e ELASTICSEARCH_PORT=<elasticsearch_port> IMAGE:latest

#### RocksDB

We provide support for [RocksDB 6.0.1](https://github.com/facebook/rocksdb/releases/tag/v6.0.1). To use RocksDB as the database, the arguments are the following:

    $ java -jar target/lazo_index-0.7.2-jar-with-dependencies.jar -p <port> -db rocksdb -rd <rocksdb_directory>

where `<rocksdb_directory>` is the data directory for RocksDB.

If you are running on a Docker image:

    $ docker run -e PORT=<port> -e DATABASE=rocksdb -e ROCKSDB_DATA_PATH=<rocksdb_directory> IMAGE:latest

## Client

### How to install?

    $ pip install lazo-index-service

### How to run?

First, instantiate the client:

```python
import lazo_index_service

lazo_client = lazo_index_service.LazoIndexClient(host=SERVER_HOST, port=SERVER_PORT)
```

where `SERVER_HOST` and `SERVER_PORT` are the hostname and the port where the Lazo index server is running, respectively.

To index a set of values:

```python
(n_permutations, hash_values, cardinality) = lazo_client.index_data(
    VALUES,
    DATASET_NAME,
    COLUMN_NAME
)
```

where `VALUES` is the list of string values, and `DATASET_NAME` and `COLUMN_NAME` are the names of the dataset/column from where the values come from. The tuple `(n_permutations, hash_values, cardinality)` represents the corresponding Lazo sketch.

If server and client are co-located, you can send the path to the dataset `csv` file instead, which should be significantly more efficient:

```python
lazo_sketches = lazo_client.index_data_path(
    DATA_PATH,
    DATASET_NAME,
    COLUMN_NAME_LIST
)
```

where `DATA_PATH` is the path to the dataset `csv` file, `DATASET_NAME` is the name of the dataset, and `COLUMN_NAME_LIST` is a list of column names. The return, `lazo_sketches`, is a list of `(n_permutations, hash_values, cardinality)` tuples, where each tuple corresponds to a dataset column.

To simply compute and retrieve the Lazo sketches, without adding them to the index, `get_lazo_sketch_from_data` or `get_lazo_sketch_from_data_path` can be used:

```python
(n_permutations, hash_values, cardinality) = lazo_client.get_lazo_sketch_from_data(
    VALUES,
    DATASET_NAME,
    COLUMN_NAME
)

lazo_sketches = lazo_client.get_lazo_sketch_from_data_path(
    DATA_PATH,
    DATASET_NAME,
    COLUMN_NAME_LIST
)
```

To query the index:

```python
query_results = lazo_client.query_data(
    QUERY_VALUES
)
```

where `QUERY_VALUES` is the list of values to be used for the query.

If server and client are co-located, and the values to be used for the query come from a dataset, you can send the path to its corresponding `csv` file instead:

```python
query_results_list = lazo_client.query_data_path(
    DATA_PATH,
    DATASET_NAME,
    COLUMN_NAME_LIST
)
```

It is also possible to delete sketches from the index:

```python
ack = lazo_client.remove_sketches(
    DATASET_NAME,
    COLUMN_NAME_LIST
)
```

where `ack` returns `True` if the requested columns are successfully deleted from the index.

### Recreating the gRPC interface

The `proto` file is located under `src/main/proto`. If you happen to change this file and need to recreate the gRPC interface for the client, run the following:

    $ pip install grpcio-tools
    $ ./compile-python.sh

The new interface will be available at `src/main/python/lazo_index`.
