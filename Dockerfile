FROM maven:3.8.2-openjdk-17 as install
WORKDIR /usr/src

COPY lib ./lib
COPY src ./src
COPY pom.xml install-repositories ./

RUN ./install-repositories && mvn package --no-transfer-progress

FROM openjdk:17-slim

ENV ELASTICSEARCH_HOST localhost
ENV ELASTICSEARCH_PORT 9200
ENV ELASTICSEARCH_INDEX lazo
ENV ROCKSDB_DATA_PATH /tmp/

ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

RUN mkdir -p /usr/src/app/home && \
    useradd -d /usr/src/app/home -s /usr/sbin/nologin -u 998 appuser && \
    chown appuser /usr/src/app/home
WORKDIR /usr/src/app
COPY --from=install /usr/src/target/lazo_index-0.7.2-jar-with-dependencies.jar lazo_index-0.7.2.jar
USER 998

ENTRYPOINT ["/tini", "--"]
ENV JAVA_OPTS="-Xms2g -Xmx2g"
CMD java $JAVA_OPTS -jar /usr/src/app/lazo_index-0.7.2.jar -p $PORT -db $DATABASE -eh $ELASTICSEARCH_HOST -ep $ELASTICSEARCH_PORT -ei $ELASTICSEARCH_INDEX -rd $ROCKSDB_DATA_PATH -pp 8000
