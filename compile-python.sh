OUTPUT=src/main/python/lazo_index_service/
python -m grpc_tools.protoc -Isrc/main/proto --python_out=$OUTPUT --grpc_python_out=$OUTPUT src/main/proto/lazo_index.proto
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    sed -i -e 's/^\(import.*_pb2\)/from . \1/' $OUTPUT/lazo_index_pb2_grpc.py
elif [[ "$OSTYPE" == "darwin"* ]]; then
    sed -i '' -e 's/^\(import.*_pb2\)/from . \1/' $OUTPUT/lazo_index_pb2_grpc.py
else
    # not tested
    sed -i '' -e 's/^\(import.*_pb2\)/from . \1/' $OUTPUT/lazo_index_pb2_grpc.py
fi
