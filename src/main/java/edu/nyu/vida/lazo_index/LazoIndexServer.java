package edu.nyu.vida.lazo_index;

import com.google.common.primitives.Longs;
import edu.nyu.vida.lazo_index.exceptions.LazoIndexServerExceptionHandler;
import edu.nyu.vida.lazo_index.exceptions.LazoInternalError;
import edu.nyu.vida.lazo_index.exceptions.LazoInvalidArgumentException;
import edu.nyu.vida.lazo_index.exceptions.LazoMalformedFileException;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import io.prometheus.client.Counter;
import io.prometheus.client.Histogram;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lazo.sketch.LazoSketch;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LazoIndexServer {

  private static final Logger logger = LoggerFactory.getLogger(LazoIndexServer.class);

  static final Counter PROM_REQUESTS =
      Counter.build()
          .name("lazo_requests_total")
          .help("Total requests.")
          .labelNames("type")
          .register();

  static final Histogram PROM_REQ_TIME =
      Histogram.build()
          .name("lazo_requests_seconds")
          .help("Time to process requests.")
          .labelNames("type")
          .register();

  private final int port;
  private final Server server;

  public LazoIndexServer(int port, LazoIndexManager lazoIndexManager) {
    this(ServerBuilder.forPort(port), port, lazoIndexManager);
  }

  public LazoIndexServer(
      ServerBuilder<?> serverBuilder, int port, LazoIndexManager lazoIndexManager) {
    this.port = port;
    server =
        serverBuilder
            .addService(new LazoIndexService(lazoIndexManager))
            .intercept(new LazoIndexServerExceptionHandler())
            .build();
  }

  public void start() throws IOException {
    server.start();
    logger.info("Server started, listening on " + port);
    Runtime.getRuntime()
        .addShutdownHook(
            new Thread(
                () -> {
                  logger.info("*** shutting down Lazo Index Server since JVM is shutting down...");
                  LazoIndexServer.this.stop();
                  logger.info("*** server shut down.");
                }));
  }

  public void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  public void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Service for indexing categorical and textual attributes using Lazo.
   *
   * @author fchirigati
   */
  private static class LazoIndexService extends LazoIndexGrpc.LazoIndexImplBase {

    private final LazoIndexManager lazoIndexManager;

    LazoIndexService(LazoIndexManager lazoIndexManager) {
      this.lazoIndexManager = lazoIndexManager;
    }

    // Internal function which backs both indexData() and getLazoSketchFromData()
    private void sketchFromData(
        ColumnValues columnValues,
        final StreamObserver<LazoSketchData> responseObserver,
        boolean indexData) {

      PROM_REQUESTS.labels("sketch_data").inc();
      Histogram.Timer timer = PROM_REQ_TIME.labels("sketch_data").startTimer();

      logger.info(
          String.format(
              "Sketching data and %s", indexData ? "storing to database" : "returning it"));

      String datasetID = columnValues.getColumnIdentifier().getDatasetId();
      String columnName = columnValues.getColumnIdentifier().getColumnName();

      final LazoSketch sketch = new LazoSketch(Utils.N_PERMUTATIONS, Utils.SKETCH_TYPE);
      for (String value : columnValues.getValuesList()) {
        sketch.update(normalize(value));
      }

      if (indexData) {
        indexDataSketch(datasetID, columnName, sketch);
      }
      responseObserver.onNext(convertToLazoSketchData(sketch));
      responseObserver.onCompleted();
      timer.observeDuration();
    }

    // Internal function which backs both indexDataPath() and getLazoSketchFromDataPath()
    private void sketchFromData(
        DataPath request, StreamObserver<LazoSketchDataList> responseObserver, boolean indexData) {

      PROM_REQUESTS.labels("sketch_path").inc();
      Histogram.Timer timer = PROM_REQ_TIME.labels("sketch_path").startTimer();

      logger.info(
          "Sketching data path=[{}] url=[{}] and {}",
          request.getPath(),
          request.getUrl(),
          indexData ? "storing to database" : "returning it");

      List<ColumnIdentifier> columnIdentifiers = request.getColumnIdentifiersList();

      String[] columnNames = getColumnNames(columnIdentifiers);
      String[] datasetIds = getDatasetIds(columnIdentifiers);
      LazoSketch[] sketch = createSketchesFromData(request);

      // Saving all the sketches
      LazoSketchData[] lazoSketchData = new LazoSketchData[columnIdentifiers.size()];
      for (int i = 0; i < columnIdentifiers.size(); i++) {
        if (indexData) {
          indexDataSketch(datasetIds[i], columnNames[i], sketch[i]);
        }
        lazoSketchData[i] = convertToLazoSketchData(sketch[i]);
      }
      responseObserver.onNext(convertToLazoSketchDataList(lazoSketchData));
      responseObserver.onCompleted();
      timer.observeDuration();
    }

    private void indexDataSketch(String datasetID, String columnName, LazoSketch sketch) {
      try {
        lazoIndexManager.addSketchToIndexAndSaveToDB(datasetID, columnName, sketch);
      } catch (Exception e) {
        throw new LazoInternalError("Exception saving sketch from data stream", e);
      }
    }

    @Override
    public void indexData(ColumnValues request, StreamObserver<LazoSketchData> responseObserver) {
      sketchFromData(request, responseObserver, true);
    }

    @Override
    public void indexDataPath(
        DataPath request, StreamObserver<LazoSketchDataList> responseObserver) {
      sketchFromData(request, responseObserver, true);
    }

    @Override
    public void getLazoSketchFromData(
        ColumnValues request, final StreamObserver<LazoSketchData> responseObserver) {
      sketchFromData(request, responseObserver, false);
    }

    @Override
    public void getLazoSketchFromDataPath(
        DataPath request, StreamObserver<LazoSketchDataList> responseObserver) {
      sketchFromData(request, responseObserver, false);
    }

    @Override
    public void removeSketches(Dataset request, StreamObserver<Ack> responseObserver) {
      PROM_REQUESTS.labels("remove").inc();
      Histogram.Timer timer = PROM_REQ_TIME.labels("remove").startTimer();

      boolean success =
          lazoIndexManager.removeSketchFromIndexAndDB(
              request.getDatasetId(), request.getColumnNamesList());

      responseObserver.onNext(Ack.newBuilder().setAck(success).build());
      responseObserver.onCompleted();
      timer.observeDuration();
    }

    @Override
    public StreamObserver<Value> queryData(
        final StreamObserver<LazoQueryResults> responseObserver) {
      PROM_REQUESTS.labels("query_data").inc();
      Histogram.Timer timer = PROM_REQ_TIME.labels("query_data").startTimer();

      return new StreamObserver<Value>() {

        final LazoSketch sketch = new LazoSketch(Utils.N_PERMUTATIONS, Utils.SKETCH_TYPE);

        public void onNext(Value value) {
          sketch.update(normalize(value.getValue()));
        }

        public void onError(Throwable t) {
          logger.warn("queryData cancelled", t);
        }

        public void onCompleted() {
          QueryResult[] results = lazoIndexManager.queryData(sketch);
          responseObserver.onNext(convertToLazoQueryResults(results));
          responseObserver.onCompleted();
          timer.observeDuration();
        }
      };
    }

    @Override
    public void queryDataPath(
        DataPath request, StreamObserver<LazoQueryResultsList> responseObserver) {

      PROM_REQUESTS.labels("query_path").inc();
      Histogram.Timer timer = PROM_REQ_TIME.labels("query_path").startTimer();

      LazoSketch[] sketch = createSketchesFromData(request);
      QueryResult[][] allQueryResults = lazoIndexManager.queryData(sketch);
      responseObserver.onNext(convertToLazoQueryResultsList(allQueryResults));
      responseObserver.onCompleted();

      timer.observeDuration();
    }

    @Override
    public void queryLazoSketchData(
        LazoSketchData request, StreamObserver<LazoQueryResults> responseObserver) {
      PROM_REQUESTS.labels("query_sketch").inc();
      Histogram.Timer timer = PROM_REQ_TIME.labels("query_sketch").startTimer();

      long[] hashValues = new long[request.getHashValuesCount()];
      for (int i = 0; i < hashValues.length; i++) {
        hashValues[i] = request.getHashValues(i);
      }

      LazoSketch sketch =
          LazoIndexManager.createSketch(
              request.getNumberPermutations(), request.getCardinality(), hashValues);

      QueryResult[] results = lazoIndexManager.queryData(sketch);
      responseObserver.onNext(convertToLazoQueryResults(results));
      responseObserver.onCompleted();
      timer.observeDuration();
    }
  }

  private static LazoQueryResultsList convertToLazoQueryResultsList(
      QueryResult[][] allQueryResults) {
    List<LazoQueryResults> lazoQueryResults = new ArrayList<>();
    for (QueryResult[] results : allQueryResults) {
      lazoQueryResults.add(convertToLazoQueryResults(results));
    }
    return LazoQueryResultsList.newBuilder().addAllColumnQueryResults(lazoQueryResults).build();
  }

  private static LazoQueryResults convertToLazoQueryResults(QueryResult[] results) {
    List<LazoQueryResult> lazoResults = new ArrayList<>();
    for (QueryResult result : results) {
      lazoResults.add(convertToLazoQueryResult(result));
    }
    return LazoQueryResults.newBuilder().addAllQueryResults(lazoResults).build();
  }

  private static LazoQueryResult convertToLazoQueryResult(QueryResult result) {
    ColumnIdentifier column =
        ColumnIdentifier.newBuilder()
            .setDatasetId(result.getDatasetId())
            .setColumnName(result.getColumnName())
            .build();
    return LazoQueryResult.newBuilder()
        .setColumn(column)
        .setMaxThreshold(result.getThreshold())
        .build();
  }

  private static LazoSketchData convertToLazoSketchData(LazoSketch sketch) {
    return LazoSketchData.newBuilder()
        .setNumberPermutations(Utils.N_PERMUTATIONS)
        .setCardinality(sketch.getCardinality())
        .addAllHashValues(Longs.asList(sketch.getHashValues()))
        .build();
  }

  private static LazoSketchDataList convertToLazoSketchDataList(LazoSketchData[] lazoSketchData) {
    return LazoSketchDataList.newBuilder()
        .addAllLazoSketchData(Arrays.asList(lazoSketchData))
        .build();
  }

  public static String normalize(String value) {
    return value.toLowerCase().trim();
  }

  private static String[] getDatasetIds(List<ColumnIdentifier> columnIdentifiers) {
    String[] datasetIds = new String[columnIdentifiers.size()];
    for (int i = 0; i < columnIdentifiers.size(); i++) {
      datasetIds[i] = columnIdentifiers.get(i).getDatasetId().trim();
    }
    return datasetIds;
  }

  private static String[] getColumnNames(List<ColumnIdentifier> columnIdentifiers) {
    String[] columnNames = new String[columnIdentifiers.size()];
    for (int i = 0; i < columnNames.length; i++) {
      columnNames[i] = columnIdentifiers.get(i).getColumnName().trim();
    }
    return columnNames;
  }

  public static LazoSketch[] createSketchesFromData(DataPath path) {
    BufferedReader reader = createBufferedReader(path);
    String[] columnNames = getColumnNames(path.getColumnIdentifiersList());
    try {
      return createSketchesFromData(reader, columnNames);
    } catch (Exception e) {
      throw new LazoMalformedFileException("Failed to create sketches from data file", e);
    }
  }

  public static LazoSketch[] createSketchesFromData(BufferedReader reader, String[] columnNames)
      throws IOException {

    // Create new LazoSketch instances
    LazoSketch[] sketches = new LazoSketch[columnNames.length];
    for (int i = 0; i < columnNames.length; i++) {
      sketches[i] = new LazoSketch(Utils.N_PERMUTATIONS, Utils.SKETCH_TYPE);
    }

    // Parse CSV and update sketches of the given columns
    CSVParser csvParser =
        new CSVParser(
            reader,
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase(false).withTrim());

    for (CSVRecord csvRecord : csvParser) {
      for (int i = 0; i < columnNames.length; i++) {
        String headerName = columnNames[i];
        if (!csvRecord.isMapped(headerName)) continue;
        String value = csvRecord.get(headerName).trim();
        if (value.isEmpty()) continue;
        sketches[i].update(normalize(value));
      }
    }
    csvParser.close();

    return sketches;
  }

  private static BufferedReader createBufferedReader(DataPath dataPath) {
    String path = dataPath.getPath();
    String url = dataPath.getUrl();
    if (isNotEmpty(path) && isNotEmpty(url)) {
      throw new LazoInvalidArgumentException("Both URL and path set");
    }
    if (isNotEmpty(path)) {
      try {
        return openFile(Paths.get(path));
      } catch (InvalidPathException e) {
        throw new LazoInvalidArgumentException("Invalid path provided: " + path);
      }
    }
    if (isNotEmpty(url)) {
      try {
        return openURI(new URI(url));
      } catch (URISyntaxException e) {
        throw new LazoInvalidArgumentException("Invalid URI provided: " + path);
      }
    }
    String msg = String.format("Neither URL nor path is set (path=[%s] url=[%s])", path, url);
    throw new LazoInvalidArgumentException(msg);
  }

  private static boolean isNotEmpty(String path) {
    return path != null && !path.isEmpty();
  }

  public static BufferedReader openURI(URI uri) {
    final String scheme = uri.getScheme();
    if (scheme.equals("file")) {
      return openFile(Paths.get(uri));
    }
    if (scheme.equals("http") || scheme.equals("https")) {
      return openURL(uri);
    }
    throw new LazoInvalidArgumentException("Unsupported URL scheme: " + scheme);
  }

  private static BufferedReader openURL(URI uri) {
    try {
      URL url = uri.toURL();
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setInstanceFollowRedirects(true);
      connection.setConnectTimeout(10 * 1000);
      int status = connection.getResponseCode();
      if (status != 200) {
        throw new IOException("HTTP error " + status + " for URL " + url);
      }
      return new BufferedReader(new InputStreamReader(connection.getInputStream()));
    } catch (IOException e) {
      String msg = String.format("Failed to read data from from URI=[%s])", uri);
      throw new LazoInvalidArgumentException(msg, e);
    }
  }

  private static BufferedReader openFile(Path path) {
    try {
      return Files.newBufferedReader(path);
    } catch (IOException e) {
      String msg = String.format("Failed to read data from file: %s", path.toAbsolutePath());
      throw new LazoInvalidArgumentException(msg, e);
    }
  }
}
