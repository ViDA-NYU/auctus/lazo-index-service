package edu.nyu.vida.lazo_index;

import edu.nyu.vida.lazo_index.storage.Storage;
import edu.nyu.vida.lazo_index.storage.StorageIterator;
import edu.nyu.vida.lazo_index.storage.StorageUnit;
import edu.nyu.vida.lazo_index.storage.elasticsearch.ElasticsearchStorage;
import edu.nyu.vida.lazo_index.storage.rocksdb.RocksDBStorage;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import lazo.index.LazoIndex;
import lazo.index.LazoIndex.LazoCandidate;
import lazo.sketch.LazoSketch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LazoIndexManager {

  // logging
  private static final Logger logger = LoggerFactory.getLogger(LazoIndexManager.class);

  // Lazo index
  private LazoIndex index = null;
  private static final ReentrantReadWriteLock indexLock = new ReentrantReadWriteLock(false);

  // Database
  private Storage storage = null;

  private void loadLazoIndex() throws Exception {
    indexLock.writeLock().lock();
    try {
      index = new LazoIndex(Utils.N_PERMUTATIONS);
    } finally {
      indexLock.writeLock().unlock();
    }

    logger.info("Initializing Lazo index...");

    if (storage.indexExists()) {

      StorageIterator storageIterator = storage.readIndex();
      int nSketches = 0;

      while (storageIterator.hasNext()) {

        StorageUnit unit = storageIterator.next();

        // update Lazo index
        LazoSketch sketch =
            createSketch(unit.getnPermutations(), unit.getCardinality(), unit.getHashValues());
        addSketchToIndex(unit.getDatasetId(), unit.getColumnName(), sketch);
        nSketches++;

        if (nSketches % 10000 == 0) {
          logger.info("Loaded {} sketches...", nSketches);
        }
      }
      logger.info("Loaded {} sketches.", nSketches);

      storageIterator.close();
      logger.info("Lazo index initialized.");
    } else {
      if (!storage.createIndex()) {
        throw new Exception("Error while creating the index in the database.");
      }
    }
  }

  public static LazoSketch createSketch(int nPermutations, long cardinality, long[] hashValues) {
    LazoSketch sketch = new LazoSketch(nPermutations, Utils.SKETCH_TYPE);
    sketch.setHashValues(hashValues);
    sketch.setCardinality(cardinality);
    return sketch;
  }

  private void saveSketchToDB(String datasetId, String columnName, LazoSketch sketch)
      throws Exception {

    StorageUnit unit =
        new StorageUnit(
            datasetId,
            columnName,
            Utils.N_PERMUTATIONS,
            sketch.getCardinality(),
            sketch.getHashValues());

    for (int i = 0; i < Utils.NUM_RETRIES - 1; ++i) {
      try {
        storage.updateIndex(unit);
        return;
      } catch (IOException e) {
        logger.warn(
            "Error while saving sketch for "
                + datasetId
                + " (column '"
                + columnName
                + "'), retrying...",
            e);
        Thread.sleep(Utils.SLEEP_TIME * 1000);
      }
    }
    storage.updateIndex(unit);
  }

  public LazoIndexManager(String db, HashMap<String, String> parameters) throws Exception {
    switch (db) {
      case "elasticsearch":
        storage = new ElasticsearchStorage(parameters);
        break;
      case "rocksdb":
        storage = new RocksDBStorage(parameters);
        break;
      default:
        throw new Exception("Database not recognizable: " + db);
    }
    loadLazoIndex();
  }

  public void close() throws Exception {
    storage.close();
  }

  public void addSketchToIndexAndSaveToDB(String datasetId, String columnName, LazoSketch sketch)
      throws Exception {
    saveSketchToDB(datasetId, columnName, sketch);
    indexLock.writeLock().lock();
    try {
      index.update(datasetId + Utils.NAME_SEPARATOR + columnName, sketch);
    } finally {
      indexLock.writeLock().unlock();
    }
  }

  public void addSketchToIndex(String datasetId, String columnName, LazoSketch sketch) {
    indexLock.writeLock().lock();
    try {
      index.update(datasetId + Utils.NAME_SEPARATOR + columnName, sketch);
    } finally {
      indexLock.writeLock().unlock();
    }
  }

  public boolean removeSketchFromIndexAndDB(String datasetId, List<String> columnNames) {
    boolean success = true;
    for (String columnName : columnNames) {
      success &= this.removeSketchFromIndexAndDB(datasetId, columnName);
    }
    return success;
  }

  public boolean removeSketchFromIndexAndDB(String datasetId, String columnName) {
    indexLock.writeLock().lock();
    try {
      index.remove(datasetId + Utils.NAME_SEPARATOR + columnName);
      storage.removeIndex(datasetId, columnName);
    } catch (Exception e) {
      logger.warn("Failed to remove sketch from index and/or DB.", e);
      return false;
    } finally {
      indexLock.writeLock().unlock();
    }
    return true;
  }

  public QueryResult[][] queryData(LazoSketch[] sketches) {
    QueryResult[][] allQueryResults = new QueryResult[sketches.length][];
    for (int i = 0; i < sketches.length; i++) {
      allQueryResults[i] = this.queryData(sketches[i]);
    }
    return allQueryResults;
  }

  public QueryResult[] queryData(LazoSketch sketch) {
    HashMap<String, Float> results = new HashMap<String, Float>();
    Set<LazoCandidate> candidates;
    indexLock.readLock().lock();
    try {
      candidates = index.queryContainment(sketch, Utils.LAZO_THRESHOLD);
    } finally {
      indexLock.readLock().unlock();
    }
    for (LazoCandidate candidate : candidates) {
      results.put((String) candidate.key, candidate.jcx);
    }

    HashMap<String, Float> resultsSorted =
        results.entrySet().stream()
            .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    QueryResult[] output = new QueryResult[results.size()];
    int id = 0;
    for (String key : resultsSorted.keySet()) {
      String[] name = key.split(Utils.NAME_SEPARATOR, 2);
      output[id] = new QueryResult(name[0], name[1], results.get(key));
      id++;
    }

    return output;
  }
}
