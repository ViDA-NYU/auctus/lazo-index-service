package edu.nyu.vida.lazo_index;

import io.prometheus.client.exporter.HTTPServer;
import io.prometheus.client.hotspot.DefaultExports;
import java.util.HashMap;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  public static Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) throws Exception {

    Options options = new Options();

    Option databaseOption =
        new Option("db", "database", true, "database to be used for the lazo server");
    databaseOption.setRequired(true);
    databaseOption.setArgName("DATABASE");
    options.addOption(databaseOption);

    Option lazoPortOption =
        new Option("p", "port", true, "port where the lazo server will be running");
    lazoPortOption.setRequired(true);
    lazoPortOption.setArgName("LAZO PORT");
    options.addOption(lazoPortOption);

    Option elasticsearchHostOption =
        new Option(
            Utils.ELASTICSEARCH_HOST_OPT,
            Utils.ELASTICSEARCH_HOST_OPT_LONG,
            true,
            "hostname where the Elasticsearch server is running");
    elasticsearchHostOption.setRequired(false);
    elasticsearchHostOption.setArgName(Utils.ELASTICSEARCH_HOST_NAME);
    options.addOption(elasticsearchHostOption);

    Option elasticsearchPortOption =
        new Option(
            Utils.ELASTICSEARCH_PORT_OPT,
            Utils.ELASTICSEARCH_PORT_OPT_LONG,
            true,
            "port where the Elasticsearch server is running");
    elasticsearchPortOption.setRequired(false);
    elasticsearchPortOption.setArgName(Utils.ELASTICSEARCH_PORT_NAME);
    options.addOption(elasticsearchPortOption);

    Option elasticsearchIndexOption =
        new Option(
            Utils.ELASTICSEARCH_INDEX_OPT,
            Utils.ELASTICSEARCH_INDEX_OPT_LONG,
            true,
            "name of Elasticsearch index to use");
    elasticsearchIndexOption.setRequired(false);
    elasticsearchIndexOption.setArgName(Utils.ELASTICSEARCH_INDEX_NAME);
    options.addOption(elasticsearchIndexOption);

    Option rocksDBDataOption =
        new Option(
            Utils.ROCKSDB_DATA_OPT,
            Utils.ROCKSDB_DATA_OPT_LONG,
            true,
            "data directory for RocksDB");
    rocksDBDataOption.setRequired(false);
    rocksDBDataOption.setArgName(Utils.ROCKSDB_DATA_NAME);
    options.addOption(rocksDBDataOption);

    Option prometheusOption =
        new Option(
            Utils.PROMETHEUS_PORT_OPT,
            Utils.PROMETHEUS_PORT_OPT_LONG,
            true,
            "port where the Prometheus metrics server will run");
    prometheusOption.setRequired(false);
    prometheusOption.setArgName(Utils.PROMETHEUS_PORT_NAME);
    options.addOption(prometheusOption);

    Option helpOption = new Option("h", "help", false, "display this message");
    helpOption.setRequired(false);
    options.addOption(helpOption);

    HelpFormatter formatter = new HelpFormatter();
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = null;

    try {
      cmd = parser.parse(options, args);
    } catch (ParseException e) {
      formatter.printHelp("java -jar <JAR FILE>", options, true);
      System.exit(0);
    }

    if (cmd.hasOption("h")) {
      formatter.printHelp("java -jar <JAR FILE>", options, true);
      System.exit(0);
    }

    HashMap<String, String> parameters = new HashMap<String, String>();
    if (cmd.hasOption(Utils.ELASTICSEARCH_HOST_OPT)) {
      parameters.put(
          Utils.ELASTICSEARCH_HOST_OPT, cmd.getOptionValue(Utils.ELASTICSEARCH_HOST_OPT));
    }
    if (cmd.hasOption(Utils.ELASTICSEARCH_PORT_OPT)) {
      parameters.put(
          Utils.ELASTICSEARCH_PORT_OPT, cmd.getOptionValue(Utils.ELASTICSEARCH_PORT_OPT));
    }
    if (cmd.hasOption(Utils.ELASTICSEARCH_INDEX_OPT)) {
      parameters.put(
          Utils.ELASTICSEARCH_INDEX_OPT, cmd.getOptionValue(Utils.ELASTICSEARCH_INDEX_OPT));
    }
    if (cmd.hasOption(Utils.ROCKSDB_DATA_OPT)) {
      parameters.put(Utils.ROCKSDB_DATA_OPT, cmd.getOptionValue(Utils.ROCKSDB_DATA_OPT));
    }
    if (cmd.hasOption(Utils.PROMETHEUS_PORT_OPT)) {
      DefaultExports.initialize();
      new HTTPServer(Integer.parseInt(cmd.getOptionValue(Utils.PROMETHEUS_PORT_OPT)));
    }

    LazoIndexManager lazoIndexManager = null;
    try {
      lazoIndexManager = new LazoIndexManager(cmd.getOptionValue("db"), parameters);
    } catch (Exception e) {
      logger.error("Failed to initialize LazoIndexManager", e);
      System.exit(1);
    }
    int port = Integer.parseInt(cmd.getOptionValue("p"));
    LazoIndexServer lazoIndexServer = new LazoIndexServer(port, lazoIndexManager);
    try {
      lazoIndexServer.start();
      lazoIndexServer.blockUntilShutdown();
    } finally {
      lazoIndexManager.close();
    }
  }
}
