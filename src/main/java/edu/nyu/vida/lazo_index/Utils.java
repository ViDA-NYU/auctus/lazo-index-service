package edu.nyu.vida.lazo_index;

import lazo.sketch.SketchType;

public class Utils {

  /*
   * Constants.
   */

  public static final int NUM_RETRIES = 5;
  public static final int SLEEP_TIME = 2;

  public static final String ELASTICSEARCH_HOST_OPT = "eh";
  public static final String ELASTICSEARCH_HOST_OPT_LONG = "ehost";
  public static final String ELASTICSEARCH_HOST_NAME = "ELASTICSEARCH HOST";

  public static final String ELASTICSEARCH_PORT_OPT = "ep";
  public static final String ELASTICSEARCH_PORT_OPT_LONG = "eport";
  public static final String ELASTICSEARCH_PORT_NAME = "ELASTICSEARCH PORT";

  public static final String ELASTICSEARCH_INDEX_OPT = "ei";
  public static final String ELASTICSEARCH_INDEX_OPT_LONG = "eindex";
  public static final String ELASTICSEARCH_INDEX_NAME = "ELASTICSEARCH INDEX";

  public static final String ROCKSDB_DATA_OPT = "rd";
  public static final String ROCKSDB_DATA_OPT_LONG = "rdata";
  public static final String ROCKSDB_DATA_NAME = "ROCKSDB DATA";

  public static final String PROMETHEUS_PORT_OPT = "pp";
  public static final String PROMETHEUS_PORT_OPT_LONG = "prometheusport";
  public static final String PROMETHEUS_PORT_NAME = "PROMETHEUS PORT";

  public static final int N_PERMUTATIONS = 256;
  public static final float LAZO_THRESHOLD = 0.1f;
  public static final SketchType SKETCH_TYPE = SketchType.MINHASH;

  public static final String NAME_SEPARATOR = "__.__";
}
