package edu.nyu.vida.lazo_index.storage;

public class StorageUnit {

  private String datasetId;
  private String columnName;
  private int nPermutations;
  private long cardinality;
  private long[] hashValues;

  public StorageUnit(
      String datasetId, String columnName, int nPermutations, long cardinality, long[] hashValues) {
    this.datasetId = datasetId;
    this.columnName = columnName;
    this.nPermutations = nPermutations;
    this.cardinality = cardinality;
    this.hashValues = hashValues;
  }

  public String getDatasetId() {
    return datasetId;
  }

  public String getColumnName() {
    return columnName;
  }

  public int getnPermutations() {
    return nPermutations;
  }

  public long getCardinality() {
    return cardinality;
  }

  public long[] getHashValues() {
    return hashValues;
  }
}
