package edu.nyu.vida.lazo_index.storage.rocksdb;

import edu.nyu.vida.lazo_index.Utils;
import edu.nyu.vida.lazo_index.storage.Storage;
import edu.nyu.vida.lazo_index.storage.StorageIterator;
import edu.nyu.vida.lazo_index.storage.StorageUnit;
import edu.nyu.vida.lazo_index.storage.elasticsearch.ElasticsearchStorage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import org.rocksdb.Options;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RocksDBStorage extends Storage {

  private Options options;
  private RocksDB db;
  private String dataFile;

  // logging
  private static final Logger logger = LoggerFactory.getLogger(ElasticsearchStorage.class);

  public RocksDBStorage(HashMap<String, String> parameters) throws Exception {
    if (!parameters.containsKey(Utils.ROCKSDB_DATA_OPT)) {
      throw new Exception("Data for RocksDB is missing");
    }
    dataFile = parameters.get(Utils.ROCKSDB_DATA_OPT).trim();
    try {
      RocksDB.loadLibrary();
      options = new Options();
      options = options.setCreateIfMissing(true);
      db = RocksDB.open(options, dataFile);
    } catch (RocksDBException e) {
      throw new Exception("Error while initializing RocksDB.", e);
    }
  }

  @Override
  public boolean indexExists() throws Exception {
    return true;
  }

  @Override
  public boolean createIndex() throws Exception {
    return true;
  }

  @Override
  public StorageIterator readIndex() throws Exception {
    return new RocksDBStorageIterator(db);
  }

  @Override
  public boolean removeIndex(String datasetId, String columnName) throws IOException {
    String printableName = String.format("[datasetId: %s columnName: %s]", datasetId, columnName);
    try {
      byte[] key = generateKey(datasetId, columnName);
      db.delete(key);
      logger.info("Deleted entry {} successfully.", printableName);
      return true;
    } catch (RocksDBException e) {
      logger.warn("Error while deleting entry " + printableName, e);
      return false;
    }
  }

  @Override
  public void updateIndex(StorageUnit data) throws Exception {
    String printableName =
        String.format("[datasetId: %s columnName: %s]", data.getDatasetId(), data.getColumnName());
    try {
      byte[] key = generateKey(data.getDatasetId(), data.getColumnName());
      byte[] value =
          generateValue(data.getnPermutations(), data.getCardinality(), data.getHashValues());
      db.put(key, value);
      logger.info("Added/updated entry {} successfully.", printableName);
    } catch (RocksDBException e) {
      throw new IOException(
          String.format("Failed while adding/updating entry %s .", printableName), e);
    }
  }

  @Override
  public void close() throws Exception {
    db.close();
  }

  public static StorageUnit keyValueToStorageUnit(byte[] key, byte[] value) {
    // key
    String[] names = getDatasetIdAndColumn(key);
    String datasetId = names[0];
    String columnName = names[1];

    // value
    long[] hashValues = new long[Utils.N_PERMUTATIONS];
    int hashValuesIndex = 0;
    for (int i = 0; i < Utils.N_PERMUTATIONS * Long.BYTES; i += Long.BYTES) {
      ByteBuffer buffer = ByteBuffer.wrap(Arrays.copyOfRange(value, i, i + Long.BYTES));
      hashValues[hashValuesIndex] = buffer.getLong();
      hashValuesIndex++;
    }
    long cardinality =
        ByteBuffer.wrap(
                Arrays.copyOfRange(
                    value,
                    Utils.N_PERMUTATIONS * Long.BYTES,
                    (Utils.N_PERMUTATIONS + 1) * Long.BYTES))
            .getLong();

    return new StorageUnit(datasetId, columnName, Utils.N_PERMUTATIONS, cardinality, hashValues);
  }

  public static String[] getDatasetIdAndColumn(byte[] id) {
    String idStr = new String(id);
    return idStr.split(Utils.NAME_SEPARATOR, 2);
  }

  private static byte[] generateKey(String datasetId, String columnName) {
    return (datasetId + Utils.NAME_SEPARATOR + columnName).getBytes();
  }

  private static byte[] generateValue(int nPermutations, long cardinality, long[] hashValues) {
    long[] hashValuesPlusCardinality = new long[hashValues.length + 1];
    System.arraycopy(hashValues, 0, hashValuesPlusCardinality, 0, hashValues.length);
    hashValuesPlusCardinality[hashValues.length] = cardinality;
    ByteBuffer bb = ByteBuffer.allocate(hashValuesPlusCardinality.length * Long.BYTES);
    bb.asLongBuffer().put(hashValuesPlusCardinality);
    return bb.array();
  }
}
