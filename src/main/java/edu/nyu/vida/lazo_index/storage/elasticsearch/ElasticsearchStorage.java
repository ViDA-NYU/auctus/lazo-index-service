package edu.nyu.vida.lazo_index.storage.elasticsearch;

import edu.nyu.vida.lazo_index.Utils;
import edu.nyu.vida.lazo_index.storage.Storage;
import edu.nyu.vida.lazo_index.storage.StorageIterator;
import edu.nyu.vida.lazo_index.storage.StorageUnit;
import io.prometheus.client.Counter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpHost;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.DocWriteResponse.Result;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElasticsearchStorage extends Storage {

  private RestHighLevelClient elasticsearchClient;
  private String host = null;
  private int port = 0;
  private String indexName;

  // logging
  private static final Logger logger = LoggerFactory.getLogger(ElasticsearchStorage.class);

  // metrics
  static final Counter PROM_ES_REQS =
      Counter.build()
          .name("lazo_elasticsearch_requests_total")
          .help("Total Elasticsearch operations.")
          .labelNames("type")
          .register();

  public ElasticsearchStorage(HashMap<String, String> parameters) throws Exception {
    indexName = parameters.getOrDefault(Utils.ELASTICSEARCH_INDEX_OPT, "lazo");
    if (!parameters.containsKey(Utils.ELASTICSEARCH_HOST_OPT)) {
      throw new Exception("Host for Elasticsearch is missing.");
    }
    if (!parameters.containsKey(Utils.ELASTICSEARCH_PORT_OPT)) {
      throw new Exception("Port for Elasticsearch " + "is missing.");
    }

    host = parameters.get(Utils.ELASTICSEARCH_HOST_OPT).trim();
    port = Integer.parseInt(parameters.get(Utils.ELASTICSEARCH_PORT_OPT).trim());
    for (int i = 0; i < 10; i++) {
      try {
        logger.info("Initializing Elasticsearch...");
        elasticsearchClient =
            new RestHighLevelClient(RestClient.builder(new HttpHost(host, port, "http")));
        if (!elasticsearchClient.ping(RequestOptions.DEFAULT)) {
          throw new Exception("Elasticsearch unavailable.");
        }
        logger.info("Elasticsearch successfully initialized.");
        return;
      } catch (Exception e) {
        logger.warn("Error while initializing elasticsearch.");
        logger.warn("Retrying...");
        TimeUnit.SECONDS.sleep(5);
      }
    }
    throw new Exception("Error while initializing elasticsearch");
  }

  @Override
  public boolean indexExists() throws IOException {
    RestClient elasticsearchRestClient =
        RestClient.builder(new HttpHost(host, port, "http")).build();
    Request request = new Request("HEAD", "/" + indexName);
    Response response = elasticsearchRestClient.performRequest(request);
    int statusCode = response.getStatusLine().getStatusCode();
    if (statusCode == 404) {
      return false;
    }
    return true;
  }

  @Override
  public boolean createIndex() throws IOException {
    logger.info("Elasticsearch index does not exist. Creating one...");
    CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName);
    CreateIndexResponse createIndexResponse =
        elasticsearchClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
    if (createIndexResponse.isAcknowledged()) {
      logger.info("Index created!");
      logger.info("Putting mapping...");
      PutMappingRequest request = new PutMappingRequest(indexName);
      XContentBuilder builder = XContentFactory.jsonBuilder();
      builder.startObject();
      {
        builder.startObject("properties");
        {
          builder.startObject("n_permutations");
          {
            builder.field("type", "text");
          }
          builder.endObject();
          builder.startObject("cardinality");
          {
            builder.field("type", "text");
          }
          builder.endObject();
          builder.startObject("date");
          {
            builder.field("type", "date");
          }
          builder.endObject();
        }
        builder.endObject();
      }
      builder.endObject();
      request.source(builder);
      AcknowledgedResponse putMappingResponse =
          elasticsearchClient.indices().putMapping(request, RequestOptions.DEFAULT);
      if (putMappingResponse.isAcknowledged()) {
        logger.info("Mapping done!");
      } else {
        logger.warn("Error with mapping!");
        return false;
      }
    } else {
      logger.warn("Index not created!");
      return false;
    }
    return true;
  }

  @Override
  public StorageIterator readIndex() throws IOException {
    return new ElasticsearchStorageIterator(elasticsearchClient, indexName);
  }

  @Override
  public boolean removeIndex(String datasetId, String columnName) throws IOException {
    PROM_ES_REQS.labels("remove").inc();

    String id = buildName(datasetId, columnName);

    DeleteRequest request = new DeleteRequest(indexName, id);
    DeleteResponse deleteResponse = elasticsearchClient.delete(request, RequestOptions.DEFAULT);

    if (deleteResponse.getResult() == DocWriteResponse.Result.DELETED) {
      logger.info("Deleted entry {} successfully.", printDocName(id));
      return true;
    }
    logger.warn(
        "Error while deleting id "
            + printDocName(id)
            + ": "
            + deleteResponse.getResult().toString());
    return false;
  }

  @Override
  public void updateIndex(StorageUnit data) throws IOException {
    PROM_ES_REQS.labels("index").inc();

    String id = buildName(data.getDatasetId(), data.getColumnName());
    String printableName =
        String.format("[datasetId: %s columnName: %s]", data.getDatasetId(), data.getColumnName());

    Map<String, Object> newData = new HashMap<String, Object>();
    newData.put("n_permutations", String.valueOf(data.getnPermutations()));
    newData.put("cardinality", String.valueOf(data.getCardinality()));
    newData.put("date", new DateTime());
    newData.put("hash", toStringArray(data.getHashValues()));

    try {
      IndexRequest indexRequest = new IndexRequest(indexName).id(id).source(newData);
      IndexResponse response = elasticsearchClient.index(indexRequest, RequestOptions.DEFAULT);

      if (!(response.getResult() == Result.CREATED || response.getResult() == Result.UPDATED)) {
        String msg =
            String.format(
                "Elasticsearch returned a non-OK response [%s] while adding entry %s to index.",
                response.getResult().toString(), printableName);
        throw new IOException(msg);
      }

      logger.info("Added/updated entry {} successfully.", printableName);

    } catch (Exception e) {
      String msg =
          String.format(
              "Failed while adding/updating entry %s to index [%s].", printableName, indexName);
      throw new IOException(msg, e);
    }
  }

  private static String[] toStringArray(long[] values) {
    String[] strings = new String[values.length];
    for (int i = 0; i < values.length; i++) {
      strings[i] = String.valueOf(values[i]);
    }
    return strings;
  }

  @Override
  public void close() throws Exception {
    elasticsearchClient.close();
  }

  private String buildName(String datasetId, String columnName) {
    return datasetId + Utils.NAME_SEPARATOR + columnName;
  }

  private String printDocName(String id) {
    String[] names = getDatasetIdAndColumn(id);
    return names[0] + ", '" + names[1] + "'";
  }

  public static String[] getDatasetIdAndColumn(String id) {
    String[] names = id.split(Utils.NAME_SEPARATOR, 2);
    if (names.length != 2) {
      throw new AssertionError("Invalid Elasticsearch document id: " + id);
    }
    return names;
  }
}
