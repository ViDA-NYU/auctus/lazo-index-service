package edu.nyu.vida.lazo_index.storage.rocksdb;

import edu.nyu.vida.lazo_index.storage.StorageIterator;
import edu.nyu.vida.lazo_index.storage.StorageUnit;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksIterator;

public class RocksDBStorageIterator extends StorageIterator {

  private RocksIterator rocksIterator;

  public RocksDBStorageIterator(RocksDB db) {
    rocksIterator = db.newIterator();
    rocksIterator.seekToFirst();
  }

  @Override
  public boolean hasNext() {
    return rocksIterator.isValid();
  }

  @Override
  public StorageUnit next() throws Exception {
    StorageUnit output =
        RocksDBStorage.keyValueToStorageUnit(rocksIterator.key(), rocksIterator.value());
    rocksIterator.next();
    return output;
  }

  @Override
  public void close() throws Exception {
    rocksIterator.close();
  }
}
