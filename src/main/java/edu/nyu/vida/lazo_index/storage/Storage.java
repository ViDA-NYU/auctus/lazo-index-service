package edu.nyu.vida.lazo_index.storage;

public abstract class Storage {

  public Storage() {}

  /** Checks if index exists in the database. */
  public abstract boolean indexExists() throws Exception;

  /** Creates an index in the database. */
  public abstract boolean createIndex() throws Exception;

  /** Returns an iterator for the values stored in the index. */
  public abstract StorageIterator readIndex() throws Exception;

  /** Removes data from the index. */
  public abstract boolean removeIndex(String datasetId, String columnName) throws Exception;

  /** Stores data in the index. */
  public abstract void updateIndex(StorageUnit data) throws Exception;

  /** Closes the database. */
  public abstract void close() throws Exception;
}
