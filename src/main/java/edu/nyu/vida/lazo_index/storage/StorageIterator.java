package edu.nyu.vida.lazo_index.storage;

public abstract class StorageIterator {

  public abstract boolean hasNext();

  public abstract StorageUnit next() throws Exception;

  public abstract void close() throws Exception;
}
