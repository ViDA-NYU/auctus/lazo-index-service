package edu.nyu.vida.lazo_index.storage.elasticsearch;

import edu.nyu.vida.lazo_index.storage.StorageIterator;
import edu.nyu.vida.lazo_index.storage.StorageUnit;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

public class ElasticsearchStorageIterator extends StorageIterator {

  private RestHighLevelClient client;
  private String scrollId;
  private SearchHit[] hits;
  private int currentHitIndex;

  private final TimeValue SCROLL_LIFETIME = TimeValue.timeValueSeconds(3600);

  public ElasticsearchStorageIterator(RestHighLevelClient client, String indexName)
      throws IOException {
    this.client = client;

    SearchRequest searchRequest = new SearchRequest(indexName);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.matchAllQuery());
    searchSourceBuilder.size(500);
    searchRequest.source(searchSourceBuilder);
    searchRequest.scroll(SCROLL_LIFETIME);
    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    scrollId = searchResponse.getScrollId();
    hits = searchResponse.getHits().getHits();
    currentHitIndex = 0;
  }

  @Override
  public boolean hasNext() {
    return (currentHitIndex < hits.length);
  }

  @SuppressWarnings("unchecked")
  @Override
  public StorageUnit next() throws IOException {
    StorageUnit unit = null;

    SearchHit hit = hits[currentHitIndex];
    Map<String, Object> source = hit.getSourceAsMap();
    if (source != null) {
      String[] names = ElasticsearchStorage.getDatasetIdAndColumn(hit.getId());
      String datasetId = names[0];
      String columnName = names[1];
      int nPermutations = Integer.valueOf((String) source.get("n_permutations"));
      long cardinality = Long.valueOf((String) source.get("cardinality"));
      ArrayList<String> hashValuesTemp = (ArrayList<String>) source.get("hash");
      long[] hashValues = new long[hashValuesTemp.size()];
      for (int j = 0; j < hashValuesTemp.size(); j++) {
        hashValues[j] = Long.valueOf(hashValuesTemp.get(j));
      }

      unit = new StorageUnit(datasetId, columnName, nPermutations, cardinality, hashValues);
    }

    // incrementing counter
    currentHitIndex++;
    if (currentHitIndex == hits.length) {
      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
      scrollRequest.scroll(SCROLL_LIFETIME);
      SearchResponse searchScrollResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchScrollResponse.getScrollId();
      hits = searchScrollResponse.getHits().getHits();
      currentHitIndex = 0;
    }

    return unit;
  }

  @Override
  public void close() throws IOException {
    ClearScrollRequest clearRequest = new ClearScrollRequest();
    clearRequest.addScrollId(scrollId);
    client.clearScroll(clearRequest, RequestOptions.DEFAULT);
  }
}
