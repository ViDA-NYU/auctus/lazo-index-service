package edu.nyu.vida.lazo_index.exceptions;

import edu.nyu.vida.lazo_index.exceptions.LazoException.ErrorType;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class intercepts all gRPC calls and handles exceptions when they are thrown. Any unexpected
 * exception finalizes the request with an INTERNAL error status and are logged.
 */
public class LazoIndexServerExceptionHandler implements io.grpc.ServerInterceptor {

  private static final Logger logger =
      LoggerFactory.getLogger(LazoIndexServerExceptionHandler.class);

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      ServerCall<ReqT, RespT> serverCall,
      Metadata metadata,
      ServerCallHandler<ReqT, RespT> serverCallHandler) {
    ServerCall.Listener<ReqT> listener = serverCallHandler.startCall(serverCall, metadata);
    return new ExceptionHandlerListener<>(listener, serverCall, metadata);
  }

  private class ExceptionHandlerListener<ReqT, RespT>
      extends ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT> {

    private ServerCall<ReqT, RespT> serverCall;
    private Metadata metadata;

    ExceptionHandlerListener(
        ServerCall.Listener<ReqT> listener, ServerCall<ReqT, RespT> serverCall, Metadata metadata) {
      super(listener);
      this.serverCall = serverCall;
      this.metadata = metadata;
    }

    @Override
    public void onHalfClose() {
      try {
        super.onHalfClose();
      } catch (Exception e) {
        handleException(e, serverCall, metadata);
      }
    }

    @Override
    public void onReady() {
      try {
        super.onReady();
      } catch (Exception e) {
        handleException(e, serverCall, metadata);
      }
    }

    private void handleException(Exception e, ServerCall<ReqT, RespT> call, Metadata metadata) {
      if (metadata == null) {
        metadata = new Metadata();
      }
      final String msg = "LazoIndexService failed with an unexpected internal error.";
      if (e instanceof LazoException) {
        LazoException error = (LazoException) e;
        if (error.type == ErrorType.INTERNAL) {
          logger.error(msg, error);
        }
        call.close(error.status(), error.metadata(metadata));
      } else {
        logger.error(msg, e);
        Status status = Status.INTERNAL.withDescription(msg).withCause(e);
        call.close(status, metadata);
      }
    }
  }
}
