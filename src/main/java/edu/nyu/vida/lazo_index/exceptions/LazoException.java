package edu.nyu.vida.lazo_index.exceptions;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;

import io.grpc.Metadata;
import io.grpc.Metadata.Key;
import io.grpc.Status;

public abstract class LazoException extends RuntimeException {

  public static final Key<String> TYPE_KEY = Key.of("lazo-error-type", ASCII_STRING_MARSHALLER);

  public final ErrorType type;

  LazoException(ErrorType type, String message) {
    this(type, message, null);
  }

  LazoException(ErrorType type, String message, Throwable cause) {
    super(message, cause);
    this.type = type;
  }

  public Status status() {
    return type.status.withDescription(this.getMessage()).withCause(this);
  }

  public Metadata metadata(Metadata metadata) {
    metadata.put(LazoException.TYPE_KEY, this.type.toString());
    return metadata;
  }

  enum ErrorType {
    INVALID_REQUEST_ARGUMENT(Status.INVALID_ARGUMENT),
    MALFORMED_FILE(Status.INVALID_ARGUMENT),
    INTERNAL(Status.INTERNAL);

    Status status;

    ErrorType(Status status) {
      this.status = status;
    }
  }
}
