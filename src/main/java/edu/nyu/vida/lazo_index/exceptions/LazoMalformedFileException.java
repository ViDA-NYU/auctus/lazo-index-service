package edu.nyu.vida.lazo_index.exceptions;

public class LazoMalformedFileException extends LazoException {

  public LazoMalformedFileException(String message, Throwable e) {
    super(ErrorType.MALFORMED_FILE, message, e);
  }
}
