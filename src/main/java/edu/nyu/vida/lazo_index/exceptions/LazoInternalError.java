package edu.nyu.vida.lazo_index.exceptions;

public class LazoInternalError extends LazoException {

  public LazoInternalError(String message, Throwable cause) {
    super(ErrorType.INTERNAL, message, cause);
  }
}
