package edu.nyu.vida.lazo_index.exceptions;

public class LazoInvalidArgumentException extends LazoException {

  public LazoInvalidArgumentException(String message) {
    super(ErrorType.INVALID_REQUEST_ARGUMENT, message);
  }

  public LazoInvalidArgumentException(String message, Throwable cause) {
    super(ErrorType.INVALID_REQUEST_ARGUMENT, message, cause);
  }
}
