package edu.nyu.vida.lazo_index;

public class QueryResult {

  private String datasetId;
  private String columnName;
  private float threshold;

  public QueryResult(String datasetId, String columnName, float threshold) {
    this.datasetId = datasetId;
    this.columnName = columnName;
    this.threshold = threshold;
  }

  public String getDatasetId() {
    return datasetId;
  }

  public String getColumnName() {
    return columnName;
  }

  public float getThreshold() {
    return threshold;
  }
}
