import os.path
import unittest

from lazo_index_service.errors import LazoInvalidArgumentError, \
    LazoUnavailableError
from lazo_index_service.client import LazoIndexClient


LAZO_HOST = os.environ.get('LAZO_HOST', 'localhost')

BASE_PATH = os.path.abspath(os.path.join(
    os.path.dirname(__file__),
    '..',
    '..',  # /src
))
DATA_PATH = os.path.join(BASE_PATH, 'test', 'resources')


class TestRequests(unittest.TestCase):
    def test_indexing_by_data_path(self):
        file_path = os.path.join(DATA_PATH, 'data.csv')
        lazo_client = LazoIndexClient(host=LAZO_HOST, port=50051)

        print('Indexing', file_path)

        lazo_client.index_data_path(file_path, 'data.csv', ['asdf'])
        print('[indexed column `asdf`]')

        hits = lazo_client.query_data_path(file_path, 'data.csv',
                                           ['asdf', 'qwer'])
        print('[query `asdf` and `qwer`]', hits)
        self.assertEqual(len(hits[0]), 1)
        self.assertEqual(len(hits[1]), 0,
                         "should not have indexed column 'qwer'")

        lazo_client.index_data_path(file_path, 'data.csv', ['qwer'])
        print('[indexed column `qwer`]')

        hits = lazo_client.query_data_path(file_path, 'data.csv',
                                           ['asdf', 'qwer'])
        print('[query `asdf` and `qwer`]', hits)
        self.assertEqual(len(hits[0]), 1)
        self.assertEqual(len(hits[1]), 1)

        success = lazo_client.remove_sketches('data.csv', ['asdf'])
        print('[Remove asdf output]', success)
        self.assertTrue(success)

        hits = lazo_client.query_data_path(file_path, 'data.csv',
                                           ['asdf', 'qwer'])
        self.assertEqual(len(hits[0]), 0, "'asdf' should have been removed")
        self.assertEqual(len(hits[1]), 1)

        success = lazo_client.remove_sketches('data.csv', ['qwer'])
        hits = lazo_client.query_data_path(file_path, 'data.csv',
                                           ['asdf', 'qwer'])
        self.assertTrue(success)
        self.assertEqual(len(hits[0]), 0, "'asdf' should have been removed")
        self.assertEqual(len(hits[1]), 0, "'qwer' should have been removed")
        print('[Remove qwer output]', success)

    def test_indexing_by_data(self):
        data = {'asdf': ['a1', 'a2', 'a3'], 'qwer': ['b1', 'b2', 'b3']}
        lazo_client = LazoIndexClient(host=LAZO_HOST, port=50051)

        print('Indexing')

        lazo_client.index_data(data['asdf'], 'data', 'asdf')
        print('[indexed column `asdf`]')

        hits = (
            lazo_client.query_data(data['asdf']),
            lazo_client.query_data(data['qwer']),
        )
        print('[query `asdf` and `qwer`]', hits)
        self.assertEqual(len(hits[0]), 1)
        self.assertEqual(len(hits[1]), 0,
                         "should not have indexed column 'qwer'")

        lazo_client.index_data(data['qwer'], 'data', 'qwer')
        print('[indexed column `qwer`]')

        hits = (
            lazo_client.query_data(data['asdf']),
            lazo_client.query_data(data['qwer']),
        )
        print('[query `asdf` and `qwer`]', hits)
        self.assertEqual(len(hits[0]), 1)
        self.assertEqual(len(hits[1]), 1)

        success = lazo_client.remove_sketches('data', ['asdf'])
        print('[Remove asdf output]', success)
        self.assertTrue(success)

        hits = (
            lazo_client.query_data(data['asdf']),
            lazo_client.query_data(data['qwer']),
        )
        self.assertEqual(len(hits[0]), 0, "'asdf' should have been removed")
        self.assertEqual(len(hits[1]), 1)

        success = lazo_client.remove_sketches('data', ['qwer'])
        hits = (
            lazo_client.query_data(data['asdf']),
            lazo_client.query_data(data['qwer']),
        )
        self.assertTrue(success)
        self.assertEqual(len(hits[0]), 0, "'asdf' should have been removed")
        self.assertEqual(len(hits[1]), 0, "'qwer' should have been removed")
        print('[Remove qwer output]', success)

    def test_duplicate_column_headers(self):
        file_path = os.path.join(DATA_PATH, 'duplicate-column-headers.csv')
        lazo_client = LazoIndexClient(host=LAZO_HOST, port=50051)

        try:
            lazo_client.index_data_path(file_path, 'data.csv', ['asdf'])
            self.fail("Should have thrown an exception when file has "
                      + "duplicate column headers")
        except LazoInvalidArgumentError as e:
            self.assertEqual(e.error_type(), 'MALFORMED_FILE')

    def test_server_unavailable_error(self):
        lazo_host = 'a.random.host.that.is.not.supposed.to.exist'
        lazo_client = LazoIndexClient(host=lazo_host, port=50051)

        def do_call():
            file_path = os.path.join(DATA_PATH, 'data.csv')
            lazo_client.index_data_path(file_path, 'data.csv', ['asdf'])

        self.assertRaises(LazoUnavailableError, do_call)


if __name__ == '__main__':
    unittest.main()
